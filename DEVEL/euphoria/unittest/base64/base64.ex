-- FILE:     BASE64.EX
-- PROJECT:  Lib2 (Unit testing)
-- PURPOSE:  Verify the results of BASE64.E library.
-- AUTHOR:   Shian Lee
-- VERSION:  1.01  October/21/2019
-- LANGUAGE: Euphoria version 3.1.1 (http://www.RapidEuphoria311.com)
-- PLATFORM: Any
-- LICENCE:  Free. Use at your own risk.
-- NOTE:     * results are compared to https://www.base64encode.org/
--           * Created using EDU 2.33.
-- HISTORY:  1.00: Initial version.
--	     1.01: four more tests for 64 and 76 line length.
-----------------------------------------------------------------------------


--------------------------- Start of user defined ---------------------------

constant
    AUTO_DELAY = 1,     -- 0 or 1 (run manual or auto); will pause on error
    DELAY_TIME = 5      -- (50 = half second, 100 = one second delay)

---------------------------- End of user defined ----------------------------


include machine.e
include graphics.e

-- Lib2 1.40+
include machine2.e
include string.e
include base64.e


boolean error

error = FALSE -- no error yet


tick_rate(100) -- useful only for DOS clock


constant
    EOF = -1,    -- file I/O return value
    SCREEN = 1,  -- device number
    ENTER = iif(platform() = LINUX, 10, 13), -- Enter key
    ESCAPE = 27, -- Esc key
    CRLF = {13, 10} -- end-of-line



procedure put_cr(integer bk, integer text, sequence s, boolean binary_string)
-- puts() in colors + new-line
    bk_color(bk)
    text_color(text)
    if binary_string then
	print(SCREEN, s)
    else
	puts(SCREEN, s)
    end if

    bk_color(BLACK)
    text_color(WHITE)
    puts(SCREEN, '\n')
end procedure


procedure enter_to_continue()
-- delay is set by the user
    if AUTO_DELAY then
	delay(DELAY_TIME)
    else
	put_cr(BLACK, BRIGHT_CYAN, "* Press ENTER key to continue... ", 0)
	clear_keyboard()
	while get_key() != ENTER do
	end while
	put_cr(BLACK, WHITE, EMPTY, 0)
    end if
end procedure


function safe(integer char)
-- return safe printable ASCII characters, or ' '
    if char > 31 and char < 127 then
	return char
    else
	return ' '
    end if
end function



procedure compare_non_equal(sequence s1, sequence s2, sequence var_name)
-- if strings are not equal, then set error flag, display errors and pause
    integer key

    error = TRUE -- set local error flag

    put_cr(BLACK, CYAN, "----" & var_name & ": ----", 0)

    for i = 1 to iif(length(s1) <= length(s2), length(s1), length(s2)) do
	-- compare byte by byte
	if s1[i] != s2[i] then

	    -- display error
	    put_cr(BLACK, YELLOW,
		sprintf(
		"s1 != s2 at byte %d: %3d != %-3d ('%s' != '%s')" &
		" | <Enter=OK> <Esc=stop>",
		{i, s1[i], s2[i], safe(s1[i]), safe(s2[i])}), 0)

	    -- press Enter to see next error
	    clear_keyboard()
	    key = EOF
	    while key != ENTER and key != ESCAPE do
		key = get_key()
	    end while
	    if key = ESCAPE then
		exit -- don't display more different bytes
	    end if
	end if
    end for

    -- pause *anyway* in case of length 0
    put_cr(BLACK, BRIGHT_CYAN, "Done compare... ", 0)
    pause()
end procedure



procedure put_results(integer test_num, sequence var_name, boolean bin_input,
-- print results on the screen
		    sequence input, sequence output,
		    sequence encoded, sequence decoded)
    sequence tmp

    -- print test title
    tmp = sprintf(
	"\n--------------- TEST %d [%s] -------------------",
	{test_num, var_name})
    put_cr(BLACK, WHITE, tmp, 0)

    -- print results of test
    put_cr(BLACK, WHITE, var_name & ": input & decoded", 0)
    put_cr(BLUE,    BRIGHT_WHITE, input, bin_input)
    put_cr(MAGENTA, BRIGHT_WHITE, decoded, bin_input)
    put_cr(BLACK, WHITE, var_name & ": output & encoded", 0)
    put_cr(CYAN,  BLACK, output, 0)
    put_cr(WHITE, BLACK, encoded, 0)

    -- compare 'output' to 'encoded'
    tmp = sprintf(
	"equal(output, encoded) = %s     Length(output, encoded): {%d, %d}",
	{equal(output, encoded) + '0', length(output), length(encoded)})
    if equal(output, encoded) then
	put_cr(GREEN, BRIGHT_WHITE, "OK. " & tmp, 0)
    else
	put_cr(RED,   YELLOW, tmp, 0)
	compare_non_equal(output, encoded, var_name)
    end if
    put_cr(BLACK, WHITE, EMPTY, 0)

    -- compare 'input' to 'decoded'
    tmp = sprintf(
	"equal(input, decoded) = %s     Length(input, decoded): {%d, %d}",
	{equal(input, decoded) + '0', length(input), length(decoded)})
    if equal(input, decoded) then
	put_cr(GREEN, BRIGHT_WHITE, "OK. " & tmp, 0)
    else
	put_cr(RED,   BRIGHT_WHITE, tmp, 0)
	compare_non_equal(input, decoded, var_name)
    end if
    put_cr(BLACK, WHITE, EMPTY, 0)

    -- manual continue or auto-delay
    enter_to_continue()
end procedure



--bookmark: 'input' test strings (created by Euphoria);
constant ASCII_0_255 =
	{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,
	29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,
	55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,
	81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,
	105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,
	125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,
	145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,
	165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,
	185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,
	205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,
	225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,
	245,246,247,248,249,250,251,252,253,254,255}

constant ASCII_255_0 =
	{255,254,253,252,251,250,249,248,247,246,245,244,243,242,241,240,239,238,237,
	236,235,234,233,232,231,230,229,228,227,226,225,224,223,222,221,220,219,218,217,
	216,215,214,213,212,211,210,209,208,207,206,205,204,203,202,201,200,199,198,197,
	196,195,194,193,192,191,190,189,188,187,186,185,184,183,182,181,180,179,178,177,
	176,175,174,173,172,171,170,169,168,167,166,165,164,163,162,161,160,159,158,157,
	156,155,154,153,152,151,150,149,148,147,146,145,144,143,142,141,140,139,138,137,
	136,135,134,133,132,131,130,129,128,127,126,125,124,123,122,121,120,119,118,117,
	116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,
	96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,
	70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,
	44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,
	18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0}

constant RAND_1 =
	{10,127,151,150,0,116,238,48,67,57,61,8,151,154,79,89,11,204,221,96,216,149,234,
	72,189,163,232,184,14,128,242,174,174,115,80,89,213,188,134,161,153,240,12,143,
	109,67,41,41,43,88,127,164,139,204,58,125,17,248,245,235,90,224,142,163,199,16,
	206,208,39,71,7,135,186,207,12,70,14,155,53,215,44,141,153,182,182,185,87,61,
	250,240,113,251,203,87,7,121,151,183,226,101,157,62,127,69,39,152,102,185,227,
	122,145,124,30,164,31,255,61,207,18,29,250,231,76,254,209,132,255,81,251,95,142,
	5,100,178,39,104,104,32,189,140,1,49,184,198,31,25,18,71,132,252,195,220,161,
	148,131,252,231,108,143,242,89,38,131,32,123,116,13,201,254,98,250,174,235,15,
	164,98,45,68,231,56,172,34,161,61,47,160,86,52,10,86,63,103,47,214,115,20,229,
	253,164,78,84,210,101,234,230,147,9,190,251,67,153,163,138,62,151,94,74,252,89,
	130,38,238,133,51,66,141,232,219,110,144,15,219,82,144,211,140,88,110,75,149,
	197,197,80,150,110,79,210,32,165,148,124,151,233,157,12,150}

constant RAND_2 =
	{41,172,0,119,153,105,130,15,89,96,201,135,1,145,185,130,198,88,105,75,221,143,
	105,134,136,80,242,193,51,221,125,68,165,249,201,221,252,218,3,184,12,164,197,
	78,124,239,129,200,73,71,66,179,244,55,170,116,126,235,226,42,28,20,222,210,68,
	204,184,181,158,149,49,243,241,218,119,115,107,224,163,183,102,36,94,125,176,
	220,219,151,43,91,1,73,146,24,107,146,53,29,16,165,164,2,132,77,27,52,226,47,
	157,158,172,154,133,75,5,156,49,168,104,55,209,252,132,15,207,81,193,89,124,70,
	96,23,249,35,215,100,37,196,29,10,131,137,83,183,50,110,166,46,200,110,240,53,
	70,12,123,120}

constant RAND_3 =
	{196,136,93,189,194,202,155,13,68,106,150,138,230,252,33,187,215,73,246,156,123,
	65,123,108,50,102,213,214,154,60,60,204,134,115,198,205,243,196,243,149,138,196,
	47,222,136,226,103,58,176,144,12,168,169,21,45,53,247,140,22,108,209,218,134,
	246,166,103,77,203,120,9,250,75,38,230,86,27,74,240,202,6,105,139,211,39,98,75,
	53,80,125,114,47,202,223,158,152,6,206,28,5,195,108,214,136,246,160,174,42,254,
	29,202,80,92,190,130,28,12,163,250,64,65,146,28,21,10,194,181,72,245,34,253,144,
	3,87,241,246,83,66,247,51,49,160,134,68,124,167,241,78,188,170,55,179,66,253,
	31,108}

constant RAND_4 =
	{104,46,70,92,19,143,107,127,31,91,55,225,230,113,232,9,88,215,171,46,228,4,81,
	216,208,192,92,137,84,207,237,53,180,230,91,104,117,146,186,241,37,129,249,120,
	158,78,63,72,150,148,228,32,252,214,6,165,63,172,170,83,58,149,62,168,167,191,
	186,174,30,36,208,244,10,42,102,180,5,125,152,163,66,87,146,243,36,147,89,236,
	29,48,11,20,113,122,33,217,73,25,153,253,146,174,106,94,50,75,203,85,166,135,
	178,77,71,136,111,170,23,244,120,140,116,115,149,2,226,72,116,190,208,167,169,
	156,227,150,113,98,50,91,139,85,118,140,153,73,248,94,197,168,105,92,107,168,
	186,115}

constant RAND_5 =
	{214,245,0,76,201,83,75,67,138,136,231,212,15,30,131,178,152,32,25,19,20,226,
	25,161,104,216,199,88,103,108,202,203,254,133,83,160,222,15,229,137,147,138,45,
	68,153,205,61,47,157,150,11,96,175,21,93,76,176,175,231,8,156,170,227,152,125,
	101,207,57,124,101,199,199,216,26,159,26,31,217,1,117,176,79,188,128,2,138,82,
	118,138,192,162,46,25,108,95,176,185,232,23,34,71,185,105,115,138,22,237,213,
	153,72,250,65,106,46,248,145,165,177,157,160,223,63,84,52,1,186,96,251,226,77,
	189,255,240,14,113,141,84,11,11,221,35,72,255,219,188,127,246,202,77,228,125,
	53,223}

constant LINE_64 =
	"1234567890123456789012345678901234567890123456789012345678901234"

constant LINE_64_2 =
	"1234567890123456789012345678901234567890123456789012345678901234" &
	"1234567890123456789012345678901234567890123456789012345678901234"

constant LINE_76 =
	"1234567890123456789012345678901234567890123456789012345678901234567890123456"

constant LINE_76_2 =
	"1234567890123456789012345678901234567890123456789012345678901234567890123456" &
	"1234567890123456789012345678901234567890123456789012345678901234567890123456"



--bookmark: 'output' test strings encoded by https://www.base64encode.org/;
constant BASE64_0_255 =
	"AAECAwQFBgcICQoLDA0ODxAREhMUFRYXGBkaGxwdHh8gISIjJCUmJygpKissLS4vMDEy" &
	"MzQ1Njc4OTo7PD0+P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2Rl" &
	"ZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fn+AgYKDhIWGh4iJiouMjY6PkJGSk5SVlpeY" &
	"mZqbnJ2en6ChoqOkpaanqKmqq6ytrq+wsbKztLW2t7i5uru8vb6/wMHCw8TFxsfIycrL" &
	"zM3Oz9DR0tPU1dbX2Nna29zd3t/g4eLj5OXm5+jp6uvs7e7v8PHy8/T19vf4+fr7/P3+/w=="

constant BASE64_255_0 =
	"//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87Nz" &
	"MvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZ" &
	"iXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmV" &
	"kY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIx" &
	"MC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAA=="

constant BASE64_RAND_1 =
	"Cn+XlgB07jBDOT0Il5pPWQvM3WDYlepIvaPouA6A8q6uc1BZ1byGoZnwDI9tQykpK1h/pIv" &
	"MOn0R+PXrWuCOo8cQztAnRweHus8MRg6bNdcsjZm2trlXPfrwcfvLVwd5l7fiZZ0+f0UnmG" &
	"a543qRfB6kH/89zxId+udM/tGE/1H7X44FZLInaGggvYwBMbjGHxkSR4T8w9yhlIP852yP8" &
	"lkmgyB7dA3J/mL6rusPpGItROc4rCKhPS+gVjQKVj9nL9ZzFOX9pE5U0mXq5pMJvvtDmaOK" &
	"PpdeSvxZgibuhTNCjejbbpAP21KQ04xYbkuVxcVQlm5P0iCllHyX6Z0Mlg=="

constant BASE64_RAND_2 =
	"KawAd5lpgg9ZYMmHAZG5gsZYaUvdj2mGiFDywTPdfUSl+cnd/NoDuAykxU5874HISUdCs/Q" &
	"3qnR+6+IqHBTe0kTMuLWelTHz8dp3c2vgo7dmJF59sNzblytbAUmSGGuSNR0QpaQChE0bNO" &
	"IvnZ6smoVLBZwxqGg30fyED89RwVl8RmAX+SPXZCXEHQqDiVO3Mm6mLshu8DVGDHt4"

constant BASE64_RAND_3 =
	"xIhdvcLKmw1EapaK5vwhu9dJ9px7QXtsMmbV1po8PMyGc8bN88TzlYrEL96I4mc6sJAMqKkV" &
	"LTX3jBZs0dqG9qZnTct4CfpLJuZWG0rwygZpi9MnYks1UH1yL8rfnpgGzhwFw2zWiPagrir+" &
	"HcpQXL6CHAyj+kBBkhwVCsK1SPUi/ZADV/H2U0L3MzGghkR8p/FOvKo3s0L9H2w="

constant BASE64_RAND_4 =
	"aC5GXBOPa38fWzfh5nHoCVjXqy7kBFHY0MBciVTP7TW05ltodZK68SWB+XieTj9IlpTkIPzW" &
	"BqU/rKpTOpU+qKe/uq4eJND0CipmtAV9mKNCV5LzJJNZ7B0wCxRxeiHZSRmZ/ZKual4yS8tV" &
	"poeyTUeIb6oX9HiMdHOVAuJIdL7Qp6mc45ZxYjJbi1V2jJlJ+F7FqGlca6i6cw=="

constant BASE64_RAND_5 =
	"1vUATMlTS0OKiOfUDx6DspggGRMU4hmhaNjHWGdsysv+hVOg3g/liZOKLUSZzT0vnZYLYK8VX" &
	"Uywr+cInKrjmH1lzzl8ZcfH2BqfGh/ZAXWwT7yAAopSdorAoi4ZbF+wuegXIke5aXOKFu3VmU" &
	"j6QWou+JGlsZ2g3z9UNAG6YPviTb3/8A5xjVQLC90jSP/bvH/2yk3kfTXf"

constant BASE64_LINE_64 =
	"MTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0N" &
	"TY3ODkwMTIzNA=="

constant BASE64_LINE_64_2 =
	"MTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0N" &
	"TY3ODkwMTIzNDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NT" &
	"Y3ODkwMTIzNDU2Nzg5MDEyMzQ="

constant BASE64_LINE_76 =
	"MTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0N" &
	"TY3ODkwMTIzNDU2Nzg5MDEyMzQ1Ng=="

constant BASE64_LINE_76_2 =
	"MTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0N" &
	"TY3ODkwMTIzNDU2Nzg5MDEyMzQ1NjEyMzQ1Njc4OTAxMjM0NTY3ODkwMTIzNDU2Nzg5MDEyMz" &
	"Q1Njc4OTAxMjM0NTY3ODkwMTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY="



--bookmark: all input & output test strings encoded by www.base64encode.org;
constant IN_OUT = {
	-- index 3 tells if the input is a binary string (for using print())
	{"", "", FALSE}, -- = "Empty input..."
	{"*", "Kg==", FALSE},
	{"**", "Kio=", FALSE},
	{"***", "Kioq", FALSE},
	{"****", "KioqKg==", FALSE},
	{"*****", "KioqKio=", FALSE},
	{"******", "KioqKioq", FALSE},
--      {"******", "KEoqEioq", FALSE},   -- error output for debug ('E')
	{ASCII_0_255, BASE64_0_255, TRUE},
	{ASCII_255_0, BASE64_255_0, TRUE},
	{RAND_1, BASE64_RAND_1, TRUE},
	{RAND_2, BASE64_RAND_2, TRUE},
	{RAND_3, BASE64_RAND_3, TRUE},
	{RAND_4, BASE64_RAND_4, TRUE},
	{RAND_5, BASE64_RAND_5, TRUE},
	{LINE_64, BASE64_LINE_64, FALSE},
	{LINE_64_2, BASE64_LINE_64_2, FALSE},
	{LINE_76, BASE64_LINE_76, FALSE},
	{LINE_76_2, BASE64_LINE_76_2, FALSE},
	{"Encode to Base64 format", "RW5jb2RlIHRvIEJhc2U2NCBmb3JtYXQ=", FALSE},
	{"Encode to Base64 forma", "RW5jb2RlIHRvIEJhc2U2NCBmb3JtYQ==", FALSE},
	{"Encode to Base64 form", "RW5jb2RlIHRvIEJhc2U2NCBmb3Jt", FALSE},
	{"Encode to Base64 for", "RW5jb2RlIHRvIEJhc2U2NCBmb3I=", FALSE},
	{"de to Base64 format", "ZGUgdG8gQmFzZTY0IGZvcm1hdA==", FALSE},
	{"e to Base64 format", "ZSB0byBCYXNlNjQgZm9ybWF0", FALSE},
	{" to Base64 format", "IHRvIEJhc2U2NCBmb3JtYXQ=", FALSE},
	{"to Base64 format", "dG8gQmFzZTY0IGZvcm1hdA==", FALSE}
}



--bookmark: Base64 variants names;
constant VARIANTS = {
--       index      name
	{B64_MAIN, "B64_MAIN"},
	{B64_MIME, "B64_MIME"},
	{B64_UTF7, "B64_UTF7"},
	{B64_URL,  "B64_URL" },
	{B64_IMAP, "B64_IMAP"},
	{B64_PEM,  "B64_PEM" },
	{B64_YUI,  "B64_YUI" },
	{B64_PI1,  "B64_PI1" },
	{B64_PI2,  "B64_PI2" },
	{B64_URL2, "B64_URL2"}
}



procedure main()
-- runs all the tests
    sequence var, input, output, encoded, decoded, var_name
    integer  v, len_output, c63, c64
    boolean  bin_input, pad_output, change_output_63_64

    -- press any key to start
    put_cr(BLACK, WHITE, "", 0)
    clear_screen()
    put_cr(MAGENTA, BRIGHT_WHITE,
	sprintf(" * Doing [%d] tests, on [%d] Base64 variants." &
	 " (Will pause on error) * \n    -- Press any key... (Ctrl+C to stop) ",
		{length(IN_OUT), length(VARIANTS)}), 0)
    pause()


    -- note: the 'output' string is B64_MAIN from https://www.base64encode.org/
    --    so we need to adjust this string to the other similar Base64 variants
    --    (i.e. adding new-line, removing padding, and changing bytes 63-64).
    for test_all = 1 to length(VARIANTS) do

	var = VARIANTS[test_all]
	v = var[1]              -- v is Base64 variant global index
	var_name = var[2]

	-- add new-line char to 'output'?
	if v = B64_MIME then
	    len_output = 76
	elsif v = B64_PEM then
	    len_output = 64
	else
	    len_output = 0 -- FALSE
	end if

	pad_output = not find(v, {B64_MAIN, B64_MIME, B64_PEM})

	-- change bytes 63-64 in the 'output' string
	change_output_63_64 = TRUE
	   if v = B64_URL then  c63 = '-'   c64 = '_'
	elsif v = B64_IMAP then c63 = '+'   c64 = ','
	elsif v = B64_YUI then  c63 = '.'   c64 = '_'
	elsif v = B64_PI1 then  c63 = '_'   c64 = '-'
	elsif v = B64_PI2 then  c63 = '.'   c64 = '_'
	elsif v = B64_URL2 then c63 = '~'   c64 = '-'
	else
	    change_output_63_64 = FALSE
	end if

	-- do all tests for a single Base64 variant:
	for test_num = 1 to length(IN_OUT) do
	    -- get all strings (encoded/decoded)
	    bin_input = IN_OUT[test_num][3]   -- use print() instead of puts()
	    input     = IN_OUT[test_num][1]   -- created by Euphoria
	    output    = IN_OUT[test_num][2]   -- by www.base64encode.org/

	    encoded   = base64_encode(v, input)
	    decoded   = base64_decode(v, encoded)


	    -- now we need to adjust the output string from
	    -- https://www.base64encode.org/ to other variants.
	    -- (string.e library makes life easier in this case...).

	    -- add new-line char to 'output' (but not to the last line)
	    if len_output then
		output = break(output, CRLF, len_output)
	    end if

	    -- pad the 'output' string
	    if pad_output then
		output = rtrim(output, "=")
	    end if

	    -- change bytes 63-64 in the 'output' string
	    if change_output_63_64 then
		output = translate(output, "+", {c63})
		output = translate(output, "/", {c64})
	    end if

	    -- print results and compare strings to find errors
	    put_results(test_num, var_name,
		bin_input, input, output, encoded, decoded)
	end for
    end for

    if error then
	put_cr(RED, YELLOW, " <<< Done, with errors. >>>", 0)
    else
	put_cr(GREEN, BRIGHT_WHITE, " *** Done, OK. *** ", 0)
    end if
end procedure


main() -- start test



-- End of file.
