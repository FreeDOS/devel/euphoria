' FILE:     TESTDATE.BAS
' PROJECT:  Lib2 (Unit testing)
' PURPOSE:  Verify the results of DATETIME.E library using MS-VBDOS.
' AUTHOR:   Shian Lee
' VERSION:  1.00  Saturday, October/12/2019
' LANGUAGE: MS Visual Basic for MS-DOS, Professional Edition, Version 1.00
' PLATFORM: DOS32 (VBDOS runs only on DOS)
' LICENCE:  Free. Use at your own risk.
' NOTE:     * Created with MS-VBDOS PRO 1.00, on FreeDOS 1.2.
' HISTORY:  1.00: Initial version.
'---------------------------------------------------------------------------

OPTION EXPLICIT
DEFINT A-Z
OPTION BASE 1

' read eu file and calculate/create a vb file
DIM EU_OUT_FILE AS STRING
DIM VB_OUT_FILE AS STRING

EU_OUT_FILE = "EU_TEST.OUT" ' #1 EU output for compare
VB_OUT_FILE = "VB_TEST.OUT" ' #2 VB output for compare

' open eu file for read & open vb file for write (in text mode)
OPEN EU_OUT_FILE FOR INPUT AS #1
OPEN VB_OUT_FILE FOR OUTPUT AS #2


' declare local variables/array
DIM record AS STRING, s AS STRING
DIM i AS INTEGER, p1 AS INTEGER, p2 AS INTEGER
DIM n(1 TO 6) AS LONG
DIM serial AS DOUBLE


' Line example from EU_OUT_FILE:
'00001;[[{2035,-3,16,-24,175,796}]]=<<{2034,9,15,3,8,16}>>=||2034-Sep-15 Fri 03:08:16||

PRINT
PRINT "Please wait..."

' Create VB_OUT_FILE:
WHILE NOT EOF(1)
	LINE INPUT #1, record

	' copy/put not-calculated date "00001;[[{2035,-3,16,-24,175,796}]]=<<"
	PRINT #2, LEFT$(record, INSTR(1, record, "=<<") + 2);

	' store not-calculated numbers [[{y,m,d,h,t,s}]] in array
	p1 = INSTR(1, record, "[[{") + 3
	p2 = INSTR(p1, record, "}]]")
	s = MID$(record, p1, p2 - p1) + ","
	p1 = 1
	FOR i = 1 TO 6
		p2 = INSTR(p1, s, ",")
		n(i) = VAL(MID$(s, p1, p2 - p1))
		p1 = p2 + 1
	NEXT i

	' calculate/put "{2034,9,15,3,8,16}>>=||"

	' date_time serial number is: nnnn + 0.nnnn (date + 0.time)
	serial = DATESERIAL(n(1), n(2), n(3)) + TIMESERIAL(n(4), n(5), n(6))

	s = "{" + FORMAT$(YEAR(serial)) + ","
	s = s + FORMAT$(MONTH(serial)) + ","
	s = s + FORMAT$(DAY(serial)) + ","
	s = s + FORMAT$(HOUR(serial)) + ","
	s = s + FORMAT$(MINUTE(serial)) + ","
	s = s + FORMAT$(SECOND(serial)) + "}>>=||"

	PRINT #2, s;

	' get/put "2034-Sep-15 Fri 03:08:16||" (+ new-line CRLF)
	' (FORMATED DATETIME)
	PRINT #2, FORMAT$(serial, "yyyy-mmm-dd ddd hh:mm:ss||")
WEND

' close opened files
CLOSE #1
CLOSE #2

' (Now you need to exit VBDOS manually)
PRINT
PRINT "Test file '" + VB_OUT_FILE + "' created successfully..."
PRINT "(Now Exit VBDOS with Alt+F4)."

END

