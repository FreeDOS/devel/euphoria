-- FILE:     MODES.EX
-- PROJECT:  Lib2 (Unit testing)
-- PURPOSE:  Used by FILE2.EX to test dos_system() and dos_system_exec()
-- AUTHOR:   Shian Lee
-- VERSION:  1.00  January/5/2022
-----------------------------------------------------------------------------


include graphics.e
include string.e
include file2.e


sequence cmd
atom mode
integer lines


cmd = command_line()
if length(cmd) = 3 then
    mode = floor(val(cmd[3]))
else
    mode = -1
end if


if not ((mode >= 0 and mode <= 19) or (mode >= 256 and mode <= 261)) then
    printf(1, "\n Bad graphics mode: %d\n", mode)
    abort(2)
elsif not OS_DOS32 then
--  puts(1, "Not DOS platform\n")
    abort(3)
elsif graphics_mode(mode) then
--  printf(1, "Cannot change graphics mode to %d\n", mode)
    abort(4)
else    
    text_color(WHITE)
    position(2, 1)
    if mode = 3 then -- original text mode?
	lines = text_rows(28) -- test different number of lines
	printf(1, " Mode %d -- %d lines", {mode, lines})
    else
	printf(1, " Mode %d", mode)
    end if
--  ? video_config()
    abort(0)
end if

