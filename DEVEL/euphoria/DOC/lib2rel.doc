

		    Lib2 for Euphoria 3.1.1 Release Notes



 Lib2 is an extended library for Euphoria 3.1.1, created by Shian Lee.
 You can download the latest version from www.rapideuphoria311.com.


 lib2 2.0.7, February/21/2022 (RC for FreeDOS v1.3) 
 ----------------------------

 - new and modified files:
 
   lib2/euphoria/bin/:
   edu.ex       3.20 (Euphoria keywords are indexed for faster control-K help)
   eduhelp.ex   new 1.00 (create/update edu.hlp index file for control-K help)
		*Run eduhelp.ex after any update to include\*.doc files*
   edu.hlp      new (index file for control-K help, created using eduhelp.ex) 

   lib2/euphoria/include/:
   doslfn.e     1.01 (lfn_open() checks for legal "mode")


 lib2 2.0.5, February/14/2022 
 ----------------------------

 - new and modified files:
 
   lib2/euphoria/bin/:
   edu.ex       3.16 (LFN, critical-error, full-path, shell, change drive, ...)
   keywordu.e   2.65 (LFN keywords, temp_filename, debugging keywords)

   lib2/euphoria/include/:
   file2.e      1.15 (LFN, temp_filename)
   doserror.e	1.01 (set_drive_map fixed for Win-XP)
   doslfn.e     new 1.00 (LFN - Long File Name support for DOS32)
   doslfn.eu    new 1.00 (fake LFN support - smaller file)

   lib2/euphoria/doc/:
   file2.doc    1.15 (LFN, temp_filename)
   machine2.doc 1.17 (upper case constants for control-k, fix lines "--<>--")
   utf8.doc     2.11 (upper case constants for control-k)
   
   lib2/euphoria/unittest/file2/:
   file2.ex	1.30 (LFN)


 lib2 2.0.2, February/04/2022 
 ----------------------------

 - new and modified files:
 
   lib2/euphoria/bin/:
   keywordu.e   2.64 (allow_error, check_error, set_drive_map, get_drive_map)

   lib2/euphoria/include/:
   file2.e      1.12 (chdrive() triggers critical-error; using upper())
   doserror.e	new 1.00 (allow_error, check_error, set_drive_map, get_drive_map)

   lib2/euphoria/doc/:
   file2.doc    1.12 (allow_error, check_error, set_drive_map, get_drive_map)

   lib2/euphoria/unittest/file2/:
   doserror.ex	new 1.00 (test doserror.e (DOS32))


 lib2 2.01, January/26/2022 
 --------------------------

 - fixing many file permissions for Linux/FreeBSD.
 
 - modified files:
 
   lib2/euphoria/include/:
   file2.e      1.11 (full_filename() return name in actual lower/upper case)

   lib2/euphoria/unittest/file2/:
   file2.ex	1.21 (t_rename_file() test rename directory)


 lib2 2.00, January/17/2022 
 --------------------------

 - new and modified files:
 
   lib2/euphoria/bin/:
   edu.ex       3.07 (utf8 names changed, limited tick_rate(), optional sound)
   keywordu.e   2.63 (utf8 names changed, 17 new file2 routines)

   lib2/euphoria/include/:
   file2.e      1.10 (17 new routines)
   utf8.e       2.10 (utf8 global routine names changed)

   lib2/euphoria/doc/:
   file2.doc    1.10 (17 new routines)
   utf8.doc     2.10 (utf8 global routine names changed)

   lib2/euphoria/demo/utf8/:
   boxes.ex     ---- (utf8 names changed)
   mapping.ex   ---- (utf8 names changed)

   lib2/euphoria/unittest/file2/:
   file2.ex	1.20 (17 new routines, tested on all systems)
   compress.ex  1.02 (starts with OS menu, tested on all systems)
   modes.ex     new 1.00 (used by file2.ex for dos_system/_exec) 


 lib2 1.49, December/24/2021 
 ---------------------------

 - modified file:

   lib2/euphoria/bin/:
   edu.ex       3.05 (logical bug in good() fixed for unicode mode...)


 lib2 1.48, December/23/2021 
 ---------------------------

 - new, modified and *removed* files:

   lib2/:
   install.ex   1.12 (note for user to delete old files manually, etc)

   lib2/euphoria/bin/:
   edu.ex       3.04 (fixes related to DOS system, short beeps, etc)
   keywordu.e   2.62 (zip_? removed, new file2 keywords: copy_file, etc)
   zip.dll      *removed*
   zip.so       *removed* 

   lib2/euphoria/include/:
   file2.e      new 1.00 (copy_file, compare_file)
   compress.e   new 1.01 (compress_file, decompress_file)
   zip.e        *removed*

   lib2/euphoria/doc/:
   file2.doc    1.00 (copy_file, compare_file, de/compress_file)
   linux64.doc       (re-install ia32-libs after new Java version)

   lib2/euphoria/source/:
   zip/*        *removed*

   lib2/euphoria/unittest/file2/:
   file2.ex	    new 1.00 (copy_file, compare_file)
   compress.ex  new 1.00 (compress_file, decompress_file)


 lib2 1.47, November/3/2020
 --------------------------

 - new and modified files:

   lib2/euphoria/bin/:
   keywordu.e	2.61 (zip_create & zip_extract added)
   zip.dll	new  (zip 0.1.19 wrapper - shared library for Win32)
   zip.so	new  (zip 0.1.19 wrapper - shared library for Linux)

   lib2/euphoria/include/:
   zip.e	1.00 (zip 0.1.19 wrapper - for Win32 & Linux)
   japi.e	1.02 (fix small bug in open_japidll(), for Win32)

   lib2/euphoria/doc/:
   file2.doc    0.01 (* file2.e will be available in the next version of lib2)

   lib2/euphoria/source/zip/:
   zip 0.1.19 c source files for WIN32 and LINUX (new)
			- https://github.com/kuba--/zip


 lib2 1.46, October/6/2020
 -------------------------

 - new and modified files:

   lib2/euphoria/bin/:
   edu.ex	3.01 (minor bug fix - please read in the file edu.ex)
   sycoloru.e	2.16 (minor bug fix)

   lib2/euphoria/doc/:
   machine2.doc	1.16 (leave only "What's new in version 1.15" in intro)


 lib2 1.45, September/29/2020
 ----------------------------

 - new and modified files:

   lib2/euphoria/bin/:
   edu.ex	3.00 (many changes/features - please read in the file edu.ex)
   keywordu.e	2.60 (support for MS-QBASIC mainly)
   sycoloru.e	2.15 (support for MS-QBASIC mainly)

   lib2/euphoria/doc/:
   file2.doc    0.00 (* file2.e will be available in the next version of lib2)
   bsd64.doc    (new)
   linux64.doc       (about installing libc6-i386)


 lib2 1.41, October/21/2019
 --------------------------

 - modified files:

   lib2/euphoria/bin/:
   keywordu.e	2.56 (new function: rbreak)

   lib2/euphoria/include/:
   string.e	1.03 (new function: rbreak())

   lib2/euphoria/doc/:
   string.doc	1.03 (rbreak)

   lib2/euphoria/unittest/base64/:
   base64.ex	1.01 (four more tests for 64 and 76 line length)


 lib2 1.40, October/20/2019
 --------------------------

 - main caption changed for all .doc files ("Name.doc" instead of "NAME.E").

 - new and modified directories and files:

   lib2/:
   install.ex	1.10 (delete linux64.txt/gcc64.txt; Windows/DOS united)
   lib2_ver.txt	---- will be installed as euphoria/doc/lib2rel.doc

   lib2/euphoria/:
   linux64.txt &
   gcc64.txt 	---- will be installed as euphoria/doc/linux64.doc (single file)

   lib2/euphoria/bin/:
   keywordu.e	2.55 (new functions: base64_encode, base64_decode, break)

   lib2/euphoria/include/:
   base64.e	1.00 (new) (new functions: base64_encode(), base64_decode())
   machine2.e	1.12 (range_per_bits() range: 0-52)
   string.e	1.02 (new function: break(). translate() optimized)

   lib2/euphoria/doc/:
   machine2.doc	1.15 (base64_encode, base64_decode, range_per_bits)
   string.doc	1.02 (break, translate)

   lib2/euphoria/unittest/base64/: (new)
   base64.ex	1.00 (unit testing for include/base64.e)


 lib2 1.34, October/12/2019
 --------------------------

 - new directory and files:

   lib2/euphoria/unittest/datetime/: (new)
   datetime.txt	1.00 (unit testing for include/datetime.e, using VBDOS)
   testdate.ex	1.00 (unit testing for include/datetime.e, using VBDOS)
   testdate.bas	1.00 (unit testing for include/datetime.e, using VBDOS)


 lib2 1.32, September/21/2019
 ----------------------------

 - modified files:

   lib2/euphoria/bin/:
   keywordu.e	2.54 (new function percent())

   lib2/euphoria/include/:
   math.e	1.02 (new function percent())

   lib2/euphoria/doc/:
   math.doc	1.12 (new function percent())


 lib2 1.31, September/20/2019
 ----------------------------

 - modified files:

   lib2/euphoria/include/:
   machine2.e	1.11 (bug fix: sencode() return *only* byte_string type)


 lib2 1.30, September/19/2019
 ----------------------------

 - modified files:

   lib2/euphoria/include/:
   machine2.e	1.10 (encode() is now faster!)

   lib2/euphoria/doc/:
   machine2.doc	1.10 (encode() is now faster!)


 lib2 1.29, September/16/2019
 ----------------------------

 - modified files:

   lib2/euphoria/bin/:
   keywordu.e	2.53 (new machine2 routines: byte_string, sencode, encode)

   lib2/euphoria/include/:
   machine2.e	1.09 (new machine2 routines: byte_string, sencode, encode)

   lib2/euphoria/doc/:
   machine2.doc	1.09 (new machine2 routines: byte_string, sencode, encode)


 lib2 1.27, September/13/2019
 ----------------------------

 - modified files:

   lib2/euphoria/bin/:
   keywordu.e	2.52 (new math routine: shuffle)

   lib2/euphoria/include/:
   math.e	1.01 (new function: shuffle())

   lib2/euphoria/doc/:
   math.doc	1.11 (new function: shuffle())


 lib2 1.26, July/16/2019
 -----------------------

 - modified files:

   lib2/euphoria/bin/:
   edu.ex	2.33 (executes Esc-tools-search with exwc.exe for Windows-10)


 lib2 1.25, July/14/2019
 -----------------------

 - new and modified files:

   lib2/euphoria/bin/:
   edu.ex	2.32 (executes code with exwc.exe if platform()=WIN32, for Windows-10)
   edu.bat 	(runs edu.ex only with ex.exe, for DOS and Windows, not for Windows-10)
   eduw.bat 	(new) (runs edu.ex with exwc.exe, for Windows-10)

   lib2/euphoria/doc/:
   edu.doc	(new) (adds more details to ed.doc, about edu.ex)


 lib2 1.22, March/7/2018
 -----------------------

 - lib2 is now compressed using the .7z format (See http://www.7-zip.org/).

 - modified files:

   lib2/:
   install.ex	1.05 (overwriting previous installation by default)

   lib2/euphoria/bin/:
   edu.ex	2.30 (tools menu, and few other changes)
   keywordu.e	2.51 (new keyword for japi: j_centerframe)
   JAPI_jar.txt (modified)

   lib2/euphoria/include/:
   japi.e	1.01 (new routine: j_centerframe)

   lib2/euphoria/doc/:
   japi.doc	1.01 (modified - still needs work)

   lib2/euphoria/demo/japi/:
   50 example programs for JAPI v1.0.6 (short (8.3) file names, all reedited)


 lib2 1.20, February/25/2018
 ---------------------------

 - new and modified files:

   lib2/euphoria/bin/:
   edu.ex	2.25 (control-K instead of control-H, help for japi.e)
   keywordu.e	2.50 (196 new keywords for japi.e)
   edu.bat	     (ex.ex is now the default, not exwc.ex)
   japi.dll	(new) (JAPI v1.0.6 dll library for WIN32 platform)
   japi.so	(new) (JAPI v1.0.6 dll library for Linux platform)
   JAPI.jar  	(new) (optional: start the JAPI v1.0.6 Kernel by hand)
   JAPI_jar.txt (new) (readme about JAPI.jar)

   lib2/euphoria/include/:
   japi.e	1.00 (new - JAPI v1.0.6 GUI library)

   lib2/euphoria/doc/:
   japi.doc	1.00 (new - JAPI v1.0.6 GUI library user manual - alpha stage)

   lib2/euphoria/demo/japi/:
   51 example programs for JAPI v1.0.6 + demo/japi/images/ directory (new)

   lib2/euphoria/source/japi_win/:
   JAPI v1.0.6 c source files for WIN32 (new)

   lib2/euphoria/source/japi_nix/:
   JAPI v1.0.6 c source files for Linux (new)


 lib2 1.17, February/24/2018
 ---------------------------

 - modified files:

   lib2/euphoria/bin/:
   edu.ex	2.20 (control-H for context sensitive help, bug fix)


 lib2 1.16, February/20/2018
 --------------------------

 - modified files:

   lib2/euphoria/bin/:
   edu.ex	2.16 (bug fix, Esc=List/Cancel, Ctrl-F freezes cursor for RTL)


 lib2 1.15, February/10/2018
 --------------------------

 - new and modified files:

   lib2/euphoria/bin/:
   edu.ex	2.15 (support for C files, and more)
   keywordu.e	2.10 (support for C files, new: bsearch, hash, checksum)
   sycoloru.e	2.10 (support for C files)
   edu		(new) (run edu.ex for Linux)
   edu.bat	(new) (run edu.ex for DOS/Windows)
   eduexec	(new) (execute C file from menu for Linux)
   eduexec.bat	(new) (execute C file from menu for DOS/Windows)

   lib2/euphoria/include/:
   machine2.e	1.08 (bsearch, hash, checksum)

   lib2/euphoria/doc/:
   machine2.doc	1.08 (bsearch, hash, checksum)


 lib2 1.12, January/28/2018
 --------------------------

 - modified files:

   lib2/euphoria/bin/:
   edu.ex	2.10 (list-box, macro, status-line, bug fix, and more)

   lib2/euphoria/demo/:
   meval.ex	(new comment)


 lib2 1.10, January/22/2018
 --------------------------

 - new and modified files:

   lib2/:
   install.ex	1.02
   lib2_ver.txt (new)

   lib2/euphoria/bin/:
   edu.ex	2.05 (new features and bug fix)
   keywordu.e	2.02 (new keywords and removed duplicates)

   lib2/euphoria/include/:
   matheval.e	1.00 (new file)

   lib2/euphoria/doc/:
   math.doc	1.10 (math_eval)

   lib2/euphoria/demo/:
   meval.ex	(demo for math_eval function)


 lib2 1.00, January/15/2018
 --------------------------

 - initial version, includes the files:

   lib2/:
   install.ex	1.00

   lib2/euphoria/:
   linux64.txt, gcc64.txt

   lib2/euphoria/bin/:
   edu.ex	2.00
   keywordu.e	2.00
   sycoloru.e	2.00
   xkey.ex

   lib2/euphoria/include/:
   machine2.e	1.06
   string.e	1.01
   math.e	1.00
   datetime.e	1.20
   random.e	1.00
   utf8.e	2.02
   binconst.e   1.00
   cp437.e	1.00
   cp1252.e	1.00

   lib2/euphoria/doc/:
   machine2.doc	1.06
   string.doc	1.01
   math.doc	1.00
   datetime.doc	1.20
   random.doc	1.00
   utf8.doc	2.02

   lib2/euphoria/demo/utf8/:
   boxes.ex
   mapping.ex
