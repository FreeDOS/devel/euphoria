			============
			Language War
			============

 Open a full-screen DOS window (press Alt-Enter) and type:
      
      ex lw

 If you choose to record a movie of your game, it will be stored
 in lastgame.vid (if you lose) or win1.vid, win2.vid etc. (if you win).
 You can play it back with: ex playback.ex
 
 More games, for DOS, Windows, Linux and FreeBSD await you on 
 the Euphoria Web site:
      
      http://www.RapidEuphoria.com
      
      - click the "Archive" link and then click "Games"
      
 
 Object of the Game
 ------------------
 It's the year 2399. The galaxy is dominated by the evil forces of the
 C empire. Programmers' minds are being enslaved to the C language. Your 
 objective as commander of the Euphoria is to rid the galaxy of the 50 C ships
 and spread euphoria throughout the galaxy. 40 of these evil ships are regular
 C ships, 9 are the more powerful ANSI C ships, and a single ship, lurking 
 somewhere in the galaxy is the extremely powerful and terrifying C++.
 
 You will encounter other ships in the galaxy as well. These are the BASIC 
 ships and the Java ships, remnants of previous empires. While it is not 
 your prime objective to destroy these ships, they will at times attack you 
 and you will want to eliminate them. This is particularly true when they 
 threaten friendly bases and planets. 
 
 Start at the novice level -- type 'n' to start the game. At the novice level 
 the enemy ships don't move around as much, or shoot at you as often.
 When you are able to win at the novice level, move up to the expert level. 
 Type 'e' or just hit Enter for the expert level.
 
 
 Moving the Euphoria through space
 ---------------------------------
 The galaxy consists of 49 quadrants arranged in a 7x7 grid. Due to the
 curvature of space, when you travel off the edge of the galaxy you will
 arrive in the quadrant on the opposite side. At any one time you can only
 see the action taking place in the current quadrant that you are located in.
 When you move past the edge of the screen you enter an adjacent quadrant.
 
 The ship moves at a certain warp speed from 0 to 5, and a certain direction
 from 1 to 8 as shown below. 

                     directions
 
                         3
                    4    |    2
                         |   
                  5------+------1
                         |
                    6    |    8
                         7 
 
 To move at warp 3 in direction 2 you would type
 
     w 3
     2
     (or 2 w 3)
 
 i.e. entering a number from 1 to 8 by itself will set your direction.
 You cannot move in a fractional direction. Your current direction is 
 displayed in the navigation box in the lower left of the screen. This box 
 also serves to remind you of the directions for firing weapons.
 
 
 Weapon Systems (in General)
 ---------------------------
 Ships fire phasors with a certain amount of input energy. The impact of the 
 phasor diminishes with distance to the target but the enemy always loses more
 energy than was put into the phasor. A ship loses energy when it fires a
 phasor, whether the phasor hits or not. Torpedos pack a 4000-unit impact
 regardless of the distance to the target. A torpedo will be neutralized
 by a torpedo deflector, if one exists on the target ship.
 
 
 Weapons on board the Euphoria
 -----------------------------
 The Euphoria comes equipped with conventional phasors and torpedos. It can 
 also fire an extremely powerful antimatter pod. The Euphoria stores a maximum
 of 50000 units of energy, and must be refueled at either a base or a planet. 
 It carries up to 10 torpedos, 3 torpedo deflectors and 4 antimatter pods. 
 Weapons and deflectors can only be replenished at a base. They appear on 
 your console as up-arrow, semi-circle, and circular pod shapes. 
 
 You can fire a phasor with input energy from 000 to 900 units. This input 
 energy is deducted from the amount that the Euphoria has left. To fire a 
 500-unit phasor in direction 1.0 type:
      
      p 5 1 0
 
 To fire a torpedo in direction 3.9 type:
      
      t 3 9
 
 To fire an antimatter pod in direction 2.1 type:
      
      a 2 1   ... then press Enter to detonate

 Exactly 2 digits are required on directions for weapons. The '.' is
 displayed automatically for you - you don't actually type it. The torpedo, 
 pod or phasor originates from a point near the middle of the Euphoria.
 
 Occasionally, you may need more than 2-digit precision, especially when the 
 target is far away from you. You can adjust the fine control on your
 weapons by adding '<' (less than) or '>' (greater than) to the direction.
 For example, if both 1.5 and 1.6 miss the enemy, type:
 
      t 1 > 5    "tee one, greater than five"
 
 to get, effectively t 1.533. Or type:
  
      t 1 < 6    "tee one, less than six"
 
 to get t 1.566. Between any adjacent two-digit directions, you have 2, 
 equally-spaced fine control directions. This works with phasors, torpedos and 
 pods. You can also type:
 
      t > 1 5 
 or
      t < 1 6

 i.e. the placement of the '<' or '>' symbol in the direction doesn't matter, 
 as long as it comes after 't', 'a' or 'p x', and before you type the 
 second digit. When you type the second digit the weapon fires immediately.
 
 Quickly typing the correct direction is important to winning the game. It may 
 take you a while to get used to it. It's better to use the numbers across the 
 top of your keyboard, than the numeric keypad on the right. The numbers on 
 the keypad are not arranged according to the directions, and they may confuse 
 you. (The intensity of the action in this game makes it necessary to use
 keyboard input for commands and directions, rather than mouse or arrow-key 
 input.)
 
 The antimatter pod will head out from the Euphoria in the direction you have 
 chosen. It will pass through any matter in its path. At the precise moment 
 that you want it to detonate, press Enter. A powerful explosion, equivalent to
 a 1000-unit phasor blast in all directions, will spread out from the point of 
 detonation. As with a phasor, the damage inflicted will decline with distance 
 from the epicenter. All objects in the quadrant, including the Euphoria and 
 any bases or planets, will be affected. If you leave the quadrant before 
 pressing Enter, the antimatter pod will not receive your signal.
 
 The pod is clearly a double-edged sword in your arsenal. You should try to 
 detonate it in the middle of a large number of C ships and other enemy ships,
 as far away as possible from the Euphoria and any planets or bases. It's very 
 useful for destroying cloaking BASIC ships. When you have a truce with the
 BASIC ships, it's considered an act of war.

 The Euphoria initially carries one pod. Each base will provide one additional
 pod the first time you dock with it. If you fire more than one pod at one
 time, both will explode simultaneously when you press Enter. After you 
 detonate a pod, any enemy survivors can probably be polished off with some 
 weak phasor blasts (unless you leave the quadrant).

 
 Canceling a Command
 -------------------
 To cancel a command, you can stop, part way through the command, and start
 typing a new one that begins with a letter, or you can type Backspace or 'c' 
 to clear the command display.
 
 
 Freezing the Game
 -----------------
 To temporarily suspend the game type 'f'. To resume type 'c' or Backspace.
 Note: This is only allowed to answer a phone call, or go to the bathroom.
 You will not be allowed to freeze the game more than 3 times.
 
 You can also press Esc to abort the game.
 
 
 Know Your Enemy
 ---------------
 C ships (red) come in 3 different sizes. All have torpedos and conventional 
 phasors. The regular ships have 4000 units of energy and 2 torpedos. A single 
 torpedo is just enough to destroy them. A 900-unit phasor from not too far 
 way will also do the trick. The ANSI C ships are somewhat larger, have 8000 
 units of energy and carry 4 torpedos. C++ has 25000 units of energy and 
 carries 10 torpedos. C++ is the largest ship in the galaxy, and has a 
 distinctive magenta color. A couple of phasor blasts from C++ at close range
 and you are dead. Watch out! C ships will never attack other C ships, and
 their phasors and torpedos have a safety mechanism that prevents accidental
 damage to other C ships.

 BASIC ships are sometimes at TRUCE (bright blue) and will not fire at you 
 unless you fire first and break the TRUCE. Sometimes they will spontaneously 
 change to HOSTILE (dark blue and harder to see), and will start firing. They 
 also have a deadly cloaking device that can make them invisible. (If you
 watch very carefully you might see a star disappear temporarily when a 
 cloaking BASIC ship moves in front of it.) When a BASIC ship is attacked, all 
 other BASIC ships in the quadrant will fire back on the attacker. BASIC ships 
 have 2 torpedos and conventional phasors. They only have 2000 units of energy. 
 BASIC ships will never attack other BASIC ships, and their weapons use a 
 similar safety mechanism as the C ships.
 
 The Java ships (green) are very unpredictable and will shoot at anything, 
 including other Java ships. They can enter a higher dimension that lets them 
 travel from any other quadrant in the galaxy and arrive at any point in the 
 current quadrant. Their invisible phasors can travel through anything. They
 have a red spot that lights up while they are phasoring another ship. They 
 have 3000 units of energy.
 
 Enemy ships always put a fixed percentage of their remaining energy into 
 each phasor blast. A fresh ANSI C will hit you with twice the impact of a 
 regular C ship. C++ will hit you with more than 6 times the impact! 
 Enemy ships gradually weaken as they shoot more and more phasors. 
 
 
 Destroying ships
 ----------------
 Any ship, including the Euphoria, is destroyed when its energy reserves reach
 0. Enemy ships regain their full energy when you leave the quadrant, so be 
 sure to finish them off while you can. Bases and planets also regain their 
 defensive energy when you leave.
 
 
 Damage
 ------
 5 different subsystems of the Euphoria are subject to damage. The harder you
 get hit by a phasor, torpedo or pod, the greater the chance of damage. On 
 your console, you will see an estimate of the time required to repair each 
 subsystem. The engines will sometimes sustain partial damage and a recommended 
 maximum warp speed will be posted. You may exceed this speed, but only at 
 great risk of total engine failure that will take a while to fix.
 
 
 The Assembly Language space shuttle
 -----------------------------------
 Your energy is about to expire! Your engines have broken down! Enemy ships
 are shooting at you! What do you do? Panic? Of course! But then you press 's',
 to abandon the Euphoria and enter the Assembly Language shuttle. This 
 small craft carries 5000 units of energy and 1 torpedo deflector. It's your
 last hope. It can dock at a base or a planet for energy. You will get a 
 new Euphoria-class ship if you dock with a base. Fortunately, this smaller
 ship consumes much less energy than the Euphoria while moving through space.
 
 
 Docking
 -------
 The Euphoria must periodically dock at an energy source (base or planet)
 to replenish its supplies. Planets offer energy only. Bases offer energy, 
 torpedos, pods, deflectors, and quick repairs to any subsystems of the ship
 that need it. 
 
 To dock, you must bump the Euphoria into the energy source while traveling 
 at WARP 1. Moving once at WARP 1 takes about 20 seconds. Typically you will 
 cruise up to the planet or base at warp 4 or so, bump into it, and then lower 
 your speed to warp 1 and wait for the "DOCKING COMPLETED" message. The planet 
 or base will remind you to lower your speed to warp 1. You'll know you are 
 docking correctly when you see the "DOCKING IN PROGRESS" progress bar. 
 
 During these 20 seconds you must be prepared to defend yourself and the base 
 or planet from attackers. Bases can fire back at any ship that fires upon 
 them, but they only have 6000 units of defensive energy, so guard them well. 
 C ships that enter the quadrant will usually attack your base first, rather 
 than you. Planets have 7000 units of defensive energy to withstand attacks 
 but cannot fire back. To dock again at the same source you must first move 
 away from it, and then bump into it again.
 
 Bases and planets have finite reserves of energy to give you. After docking
 a few times at the same energy source, you may notice that it has dried
 up, and has not given you the full amount of energy that you expected. It is 
 time to find new sources. A base will always provide repairs and new deflector
 shields, but it has only one pod and a finite supply of energy and torpedos. 
 A base might still have some torpedos available, even when it has no energy 
 to give you.
 
 
 The Galaxy Scan
 ---------------
 Command 'g' displays a view of the entire galaxy. You can see the locations
 of planets, bases and enemy ships for any quadrant that you have visited. The 
 current quadrant is brighter, and is updated frequently so you can see the
 location of all objects including the Euphoria. Typing 'g' again will restore 
 the normal full-screen view of the current quadrant. Sometimes your sensors 
 will be damaged and you will not be able to view the galaxy scan. It also
 shows how many enemy ships are remaining, and you can find out if the BASIC 
 ships are at TRUCE, are HOSTILE, or are CLOAKING (invisible). This scan is 
 particularly important when your reserves are getting low and you are 
 searching for an energy source. As you view the scan, the action continues. 
 Your ship is still moving and you can be struck by enemy fire - watch out! 
 
 Note that when you enter a quadrant the number of enemy ships will be as
 shown on the scan, but the positions of these ships could be very different.
 Only planets and bases have fixed positions in space. Only the scan for 
 the current quadrant shows precise enemy locations.
 
 If you bump into anything while viewing the galaxy scan, it will be turned
 off immediately for your safety.

 The galaxy scan indicates a dried up energy source by showing the planet or 
 base in white. (The base might still have torpedos).

 When the BASIC ships are cloaking, you will not see any BASIC ships on the
 scan.
 
 
 Tips
 ----
 Winning this game requires your full concentration. You must act quickly and 
 decisively when there is a threat to your safety. You must keep an eye on your
 energy reserves, and plan where your next docking will take place. Do not just 
 fly around aimlessly, picking fights with whoever comes along. You need a plan.

 Be aware when your deflectors have run out and you become vulnerable to 
 4000-unit torpedo blasts. 
 
 It's a good idea to focus on quadrants that contain energy sources. Clear out 
 any enemy ships and make it a safe place to dock. 
 
 Do not have a showdown with C++ until you are physically and emotionally
 ready. It takes several torpedo hits or medium-range phasor blasts to kill 
 him. He will hit you with phasor blasts that are much stronger than you are 
 used to from other enemy craft. You might consider using a pod - but be sure 
 to detonate it very close to him.
 
 From a tactical point of view you are safer to travel near the edge of 
 quadrants, since you can quickly escape into the adjacent quadrant when 
 you are in danger. When entering a new quadrant be prepared to immediately 
 back out if you don't like what you see. 
 
 Travel at WARP 4 most of the time. It's the most energy-efficient way to
 travel. WARP 5 is faster but burns energy at a very high rate. Note that
 even at warp 0, you still use a small amount of "life support" energy.
 
 Enemy ships can enter your quadrant from the 8 neighboring quadrants. 
 (Javas can enter from any quadrant). They tend to do so when your energy 
 is low or you are moving slowly. You should anticipate their sudden arrival, 
 especially when you are attempting to dock. Be prepared to act quickly, or
 your energy source may be blown out from under you, just before you dock with
 it.
 
 You will need some practice before you are skilled at typing in commands and
 aiming your weapons. It might be a good idea to print lw.sum and have it
 beside you as you learn. 
 
 You don't have to wait for a command to finish before typing in your next 
 command. For instance, after firing a torpedo, you can fire a second, third, 
 etc. torpedo without waiting for the first to hit or miss.
  
 When shooting at an enemy target, it helps to stop at WARP 0 or, if you are 
 docking, move at WARP 1. You can shoot on the run, but it gets frustrating 
 when you miss and waste your torpedo, or phasor energy. Enemy ships tend to
 keep moving in the same direction for a little while, before changing
 direction. Sometimes it helps to shoot just ahead of where they are moving.
 If you glance at the screen just before typing the last digit, you can
 sometimes make a last-second adjustment that will result in a hit.
 
 You can fire a trial phasor with 0 units of energy to see what the correct 
 direction is before you waste energy on a real phasor. 0-unit phasors are 
 also useful for finding cloaking BASIC ships, If the Euphoria suddenly stops 
 moving, it's very likely that you have bumped into a cloaking BASIC ship. 
 Destroy it immediately before it hits you with a huge close-range phasor blast!
 
 When docking, be very careful not to hit the planet or base you are docked 
 with. Use a 0-unit phasor to test. 
 
 Do not use 900-unit phasors when something a lot less will finish off your 
 enemy. Torpedos are useful when the enemy ship is far away. Unfortunately, 
 torpedos are slower than phasors, and the enemy ship might move away before 
 the torpedo gets there.
 
 Enemy ships will always try to destroy the last ship that fired and struck
 them, but when you leave the quadrant this information is lost. You can 
 often set up situations where two enemy ships will get into a battle between
 themselves. 
 
 It is quite an achievement to win this game at the expert level. 
 
 If you think of a way of improving the game, by all means go ahead!


                 Objects in the Galaxy
		 ---------------------
 The Good Guys:
     Euphoria (yellow)
     Assembly Language Shuttle (yellow)
     Base     (yellow)
     Planet   (brown)
 
 The Bad Guys:
     C        (red)
     ANSI C   (red)
     C++      (magenta)

 The Annoying Guys:
     BASIC    (blue / dark blue / invisible)
     Java     (green)


     Good Luck!
                       
