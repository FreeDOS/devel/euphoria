
-- Evaluate a math expression using math_eval() and compare the result to
-- Euphoria interpreter (ex or exu). Note that you cannot compare 0b/0o/0x
-- numbers to Euphoria 3.1.1. -- See MATH.DOC for more details.

-- Be careful: Euphoria interpreter will execute *any* command, not only
--             math expression...

include get.e
include graphics.e
include matheval.e  -- lib2

constant FILE = "mevalTMP.exw" -- *temporary* file for demo

integer fn
sequence st
object result

while 1 do
    text_color(WHITE)
    st = prompt_string("\nEnter a math expression (q to Quit): ")
    if equal(st, "q") then
	exit
    end if

    result = math_eval(st) -- evaluate the expression

    text_color(BRIGHT_WHITE)
    puts(1, "  The result is:   ")
    if atom(result) then
	? result
    else
	text_color(BRIGHT_MAGENTA)
	puts(1, result & '\n') -- print error message
    end if

    -- compare math_eval() result to Euphoria interpreter:
    fn = open(FILE, "w")
    if fn = -1 then
	text_color(WHITE)
	puts(1, "\nCannot open the file '" & FILE & "!\n")
	abort(1)
    end if

    -- save the expression in file
    puts(fn, "include matheval.e -- functions\n\n? " & st & '\n')
    close(fn)

    -- evaluate the expression using Euphoria 3.1.1 interpreter
    text_color(BRIGHT_CYAN)
    puts(1, "  Euphoria result: ")
    if platform() = LINUX then
	system("exu " & FILE, 2)
    elsif platform() = WIN32 then -- for Windows-10
	system("exwc " & FILE, 2)
    else -- DOS32
	system("ex " & FILE, 2)
    end if
end while


-- delete temporary file
if platform() = LINUX then
    system("rm " & FILE, 2)
else -- WIN32, DOS32
    system("DEL " & FILE, 2)
end if

