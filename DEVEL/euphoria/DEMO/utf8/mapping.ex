-- print the mapping table for cp437 or cp1252 to Unicode
-- (the correct character that you see on the screen - code page or UTF-8 -
--  depends on your terminal/console encoding).
-- Update: December/27/2021

include get.e

include cp437.e
include cp1252.e
include utf8.e

atom number
sequence table, name, cp_char, utf8_char


number = prompt_number(
    "Enter a number to see the mapping table to Unicode:\n\n" &
    "1 - cp437\n" &
    "2 - cp1252\n" &
    "3 - Quit\n\n", {1, 3}
)

if number = 1 then
    table = 0 & CP437
    name = "cp437"
elsif number = 2 then
    table = 0 & CP1252
    name = "cp1252"
else
    abort(0)
end if


clear_screen()
for i = 0 to 255 do
    -- pause after each screen
    if i and not remainder(i, 16) then
	puts(1, "...\n")
	if wait_key() then end if
    end if

    -- characters to print on the screen (don't print control characters)
    if i <= 31 or i = 127 then
	cp_char = " "
	utf8_char = " "
    else
	cp_char = {i}
	utf8_char = unicode_to_utf8({table[1 + i]})
    end if

    printf(1, "%s:  %03d  '%s'    -->   Unicode 'UTF-8':  #%04x  '%s'\n",
	{name, i, cp_char, table[1 + i], utf8_char}
    )
end for
