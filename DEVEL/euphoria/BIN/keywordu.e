-- (c) Copyright 2007 Rapid Deployment Software - See License.txt
--
-- Euphoria 3.1
-- Keywords and routines built in to ex, exw and exu

-- Version: keywordu.e 2.65 for Euphoria 3.1.1, February/13/2022, Shian Lee.
--
-- What's new in version 2.65?
--  * new in Euphoria: temp_filename, allow_lfn, lfn_short_path, lfn_long_path, 
--    check_lfn, lfn_current_dir, lfn_chdir, lfn_dir, lfn_open
--  * new/moved to Euphoria user_words: profile, profile_time, trace, warning,
--    type_check
--
-- Details: keywordu.e is used by edu.ex and sycoloru.e. It supports two
--          more groups of keywords: 'libraries' (for library routines) and
--          'user_words' (for your routines).
--          It also supports Standard C keywords, which is useful for
--          wrapping C libraries, and QBASIC keywords.


--bookmark: Euphoria 3.1.1 and Lib2; ----------------------------------------

global constant keywords = {
    "if", "end", "then", "procedure", "else", "for", "return", "do", "elsif",
    "while", "type", "constant", "to", "and", "or", "exit", "function",
    "global", "by", "not", "include", "xor", "with", "without"}

global constant builtins = {
    "length", "puts", "integer", "sequence", "position", "object", "append",
    "prepend", "print", "printf", "clear_screen", "floor", "getc", "gets",
    "get_key", "rand", "repeat", "atom", "compare", "find", "match", "time",
    "command_line", "open", "close", "getenv", "sqrt", "sin", "cos", "tan",
    "log", "system", "date", "remainder", "power", "sprintf", "machine_func",
    "machine_proc", "abort", "peek", "poke", "call", "arctan", "and_bits",
    "or_bits", "xor_bits", "not_bits", "pixel", "get_pixel", "mem_copy",
    "mem_set", "c_proc", "c_func", "routine_id", "call_proc", "call_func",
    "poke4", "peek4s", "peek4u", "equal", "system_exec", "platform",
    "task_create", "task_schedule", "task_yield", "task_self", "task_suspend",
    "task_status", "task_clock_stop", "task_clock_start", "task_list",
    "find_from", "match_from", "$", "?"}

global constant libraries = {
-- Library functions - from euphoria/doc/library.doc
    "reverse", "sort", "custom_sort", "lower", "upper", "wildcard_match",
    "wildcard_file", "arcsin", "arccos", "PI", "flush", "lock_file",
    "unlock_file", "pretty_print", "sprint", "get_bytes", "prompt_string",
    "wait_key", "get", "prompt_number", "value", "seek", "where",
    "current_dir", "chdir", "dir", "walk_dir", "allow_break", "check_break",
    "get_mouse", "mouse_events", "mouse_pointer", "tick_rate", "sleep",
    "get_position", "graphics_mode", "video_config", "scroll", "wrap",
    "text_color", "bk_color", "palette", "all_palette", "get_all_palette",
    "read_bitmap", "save_bitmap", "get_active_page", "set_active_page",
    "get_display_page", "set_display_page", "sound", "cursor", "text_rows",
    "get_screen_char", "put_screen_char", "save_text_image",
    "display_text_image", "draw_line", "polygon", "ellipse", "save_screen",
    "save_image", "display_image", "dos_interrupt", "allocate", "free",
    "allocate_low", "free_low", "allocate_string", "register_block",
    "unregister_block", "get_vector", "set_vector", "lock_memory",
    "int_to_bytes", "bytes_to_int", "int_to_bits", "bits_to_int",
    "atom_to_float64", "atom_to_float32", "float64_to_atom", "float32_to_atom",
    "set_rand", "use_vesa", "crash_file", "crash_message", "crash_routine",
    "open_dll", "define_c_proc", "define_c_func", "define_c_var", "call_back",
    "message_box", "free_console", "instance",
-- Color constants - from euphoria/include/graphics.e
    "BLACK", "GREEN", "MAGENTA", "WHITE", "GRAY", "BRIGHT_GREEN",
    "BRIGHT_MAGENTA", "BRIGHT_WHITE", "BLUE", "CYAN", "RED", "BROWN",
    "BRIGHT_BLUE", "BRIGHT_CYAN", "BRIGHT_RED", "YELLOW",
-- video_config() constants - from euphoria/include/graphics.e
    "VC_COLOR", "VC_MODE", "VC_LINES", "VC_COLUMNS", "VC_XPIXELS",
    "VC_YPIXELS", "VC_NCOLORS", "VC_PAGES",
-- dir() constants - from euphoria/include/file.e
    "D_NAME", "D_ATTRIBUTES", "D_SIZE", "D_YEAR", "D_MONTH", "D_DAY",
    "D_HOUR", "D_MINUTE", "D_SECOND",
-- Library functions - from euphoria/doc/database.doc
    "db_create", "db_open", "db_select", "db_close", "db_create_table",
    "db_select_table", "db_rename_table", "db_delete_table",
    "db_table_list", "db_table_size", "db_find_key", "db_record_key",
    "db_record_data", "db_insert", "db_delete_record",
    "db_replace_data", "db_compress", "db_dump", "db_fatal_id",
-- Lib2 functions - from euphoria/doc/machine2.doc
    "boolean", "dwords", "string", "sum", "flat", "flatten", "EMPTY", "abs",
    "fix", "sign", "psign", "nsign", "zsign", "eqv_bits", "imp_bits",
    "shl_bits", "shr_bits", "rol_bits", "ror_bits", "get_bit", "set_bit",
    "pack_bits", "unpack_bits", "FALSE", "TRUE", "putx", "get_xkey",
    "wait_xkey", "pause", "clear_keyboard", "delay", "TIME_SIGN", "peek2s",
    "peek2u", "poke2", "bin", "oct", "dec", "hex", "int", "signed", "unsigned",
    "range_per_bits", "crash", "iif", "bsearch", "hash", "checksum", "encode",
    "sencode", "byte_string", "base64_encode", "base64_decode",
-- Lib2 functions - from euphoria/doc/string.doc
    "spc", "chr", "lset", "rset", "mset", "quote", "str", "val", "WSPACE",
    "ltrim", "rtrim", "trim", "rfind", "rfind_from", "rmatch", "rmatch_from",
    "left", "right", "left_of", "right_of", "mid", "change", "translate",
    "squeeze", "count_words", "match_words", "split", "join", "break", "rbreak",
-- Lib2 functions - from euphoria/doc/math.doc
    "MINF", "PINF", "E", "PHI", "EULER_GAMMA", "HALFSQRT2", "SQRT2", "SQRT3",
    "SQRT5", "SQRTE", "LN2", "LN10", "INVLN2", "INVLN10", "QUARTPI", "HALFPI",
    "TWOPI", "PISQR", "INVSQ2PI", "DEG_TO_RAD", "RAD_TO_DEG", "comp",
    "comp_approx", "is_int", "is_even", "is_odd", "is_prime", "gcd", "lcm",
    "min", "max", "product", "average", "ceil", "round", "round_to", "fixup",
    "even", "odd", "mod", "frac", "log10", "exp", "fib", "rand_range", "rnd",
    "csc", "sec", "cot", "atan2", "arccsc", "arcsec", "arccot", "deg_to_rad",
    "rad_to_deg", "sinh", "cosh", "tanh", "csch", "sech", "coth", "arcsinh",
    "arccosh", "arctanh", "arccsch", "arcsech", "arccoth", "serial_numbers",
    "prime_range", "prime_list", "math_eval", "shuffle", "percent",
-- Lib2 functions - from euphoria/doc/datetime.doc
    "now", "datetime", "dateinfo", "datediff", "sprintd",
-- Lib2 functions - from euphoria/doc/random.doc
    "ra_open", "ra_length", "ra_put", "ra_get", "ra_close",
-- Lib2 functions - from euphoria/doc/utf8.doc
    "utf8_to_unicode", "unicode_to_utf8", "group_utf8", "UNICODE_BOM",
    "UTF8_BOM", "UNICODE_REP", "UTF8_REP", "CP437", "CP1252",
-- Lib2 functions - from euphoria/doc/file2.doc
    "compress_file", "decompress_file", "copy_file", "compare_file", "files",
    "current_drive", "chdrive", "get_drives", "make_dir", "remove_dir",
    "rename_file", "delete_file", "open_libc", "open_libm", "full_dirname",
    "full_filename", "parse_path", "full_pathname", "slash_path", "dos_system",
    "dos_system_exec", "allow_error", "check_error", "set_drive_map", 
    "get_drive_map", "temp_filename", "allow_lfn", "check_lfn","lfn_chdir",
    "lfn_current_dir", "lfn_dir", "lfn_short_path", "lfn_long_path", "lfn_open",
-- Lib2 functions - from euphoria/doc/japi.doc - (GUI Components)
    "j_button", "j_borderpanel", "j_canvas", "j_checkbox", "j_checkmenuitem",
    "j_choice", "j_dialog", "j_focuslistener", "j_frame", "j_helpmenu",
    "j_hscrollbar", "j_graphicbutton", "j_graphiclabel", "j_image",
    "j_keylistener", "j_label", "j_led", "j_list", "j_menu", "j_menuitem",
    "j_meter", "j_mouselistener", "j_panel", "j_popupmenu", "j_printer",
    "j_progressbar", "j_radiobutton", "j_sevensegment", "j_scrollpane",
    "j_textarea", "j_textfield", "j_vscrollbar", "j_window",
-- Lib2 functions - from euphoria/doc/japi.doc - (GUI Routines)
    "j_additem", "j_add", "j_alertbox", "j_appendtext", "j_beep",
    "j_choicebox2", "j_choicebox3", "j_cliprect", "j_componentlistener",
    "j_connect", "j_delete", "j_deselect", "j_disable", "j_dispose",
    "j_drawarc", "j_drawcircle", "j_drawimagesource", "j_drawimage",
    "j_drawline", "j_drawoval", "j_drawpixel", "j_drawpolygon",
    "j_drawpolyline", "j_drawrect", "j_drawroundrect", "j_drawscaledimage",
    "j_drawstring", "j_enable", "j_filedialog", "j_fileselect", "j_fillarc",
    "j_fillcircle", "j_filloval", "j_fillpolygon", "j_fillrect",
    "j_fillroundrect", "j_getaction",  "j_getcolumns", "j_getcurpos",
    "j_getdanger", "j_getfontascent", "j_getfontheight", "j_getheight",
    "j_getimagesource", "j_getimage", "j_getinheight", "j_getinsets",
    "j_getinwidth", "j_getitemcount", "j_getitem", "j_getkeychar",
    "j_getkeycode", "j_getlayoutid", "j_getlength", "j_getmousebutton",
    "j_getmousepos", "j_getmousex", "j_getmousey", "j_getparentid",
    "j_getpos", "j_getrows", "j_getscaledimage", "j_getscreenheight",
    "j_getscreenwidth", "j_getselect", "j_getselend", "j_getselstart",
    "j_getseltext", "j_getstate", "j_getstringwidth", "j_gettext", "j_getvalue",
    "j_getviewportheight", "j_getviewportwidth", "j_getwidth", "j_getxpos",
    "j_getypos", "j_hasfocus","j_hide", "j_insert", "j_inserttext",
    "j_isparent", "j_isresizable", "j_isselect", "j_isvisible", "j_kill",
    "j_line", "j_loadimage", "j_menubar", "j_messagebox", "j_multiplemode",
    "j_nextaction", "j_pack", "j_play", "j_playsoundfile", "j_print", "j_quit",
    "j_radiogroup", "j_random", "j_releaseall", "j_release", "j_removeall",
    "j_removeitem", "j_remove", "j_replacetext", "j_saveimage", "j_selectall",
    "j_select", "j_selecttext", "j_seperator", "j_setalign", "j_setblockinc",
    "j_setborderlayout", "j_setborderpos", "j_setcolorbg", "j_setcolor",
    "j_setcolumns", "j_setcurpos", "j_setcursor", "j_setdanger", "j_setdebug",
    "j_setechochar", "j_seteditable", "j_setfixlayout", "j_setflowfill",
    "j_setflowlayout", "j_setfocus", "j_setfontname", "j_setfontsize",
    "j_setfontstyle", "j_setfont", "j_setgridlayout", "j_sethgap", "j_seticon",
    "j_setimage", "j_setinsets", "j_setmax", "j_setmin", "j_setnamedcolorbg",
    "j_setnamedcolor", "j_setnolayout", "j_setport", "j_setpos",
    "j_setradiogroup", "j_setresizable", "j_setrows", "j_setshortcut",
    "j_setsize", "j_setslidesize", "j_setstate", "j_settext", "j_setunitinc",
    "j_setvalue", "j_setvgap", "j_setxor", "j_showpopup", "j_show", "j_sleep",
    "j_sound", "j_start", "j_sync", "j_translate", "j_windowlistener",
-- Lib2 functions - from euphoria/doc/japi.doc - (GUI Routines by Shian)
    "j_centerframe"}


global constant user_words = {
-- top level keywords for Debugging and Profiling
    "profile", "profile_time", "trace", "warning", "type_check"
}



--bookmark: Standard C (and C++); -------------------------------------------

global constant c_keywords = {
    "for", "if", "while", "do", "else", "case", "default", "switch", "try",
    "throw", "catch", "operator", "new", "delete", "goto", "continue", "break",
    "return"}

global constant c_builtins = {
    -- builtin types
    "float", "double", "bool", "char", "int", "short", "long", "sizeof",
    "enum", "void", "auto", "static", "const", "struct", "union", "typedef",
    "extern", "signed", "unsigned", "inline",
    -- builtin scope
    "class", "namespace", "template", "public", "protected", "private",
    "typename", "this", "friend", "virtual", "using", "mutable", "volatile",
    "register", "explicit",
    -- GCC builtins
    "__attribute__", "__aligned__", "__asm__", "__builtin__", "__hidden__",
    "__inline__", "__packed__", "__restrict__", "__section__", "__typeof__",
    "__weak__"}

global constant c_libraries = { -- Standard C Library
    -- assert.h
    "assert",
    -- ctype.h
    "isalnum", "isalpha", "iscntrl", "isdigit", "isgraph", "islower", "isprint",
    "ispunct", "isspace", "isupper", "isxdigit", "tolower", "toupper",
    -- errno.h
    "EDOM", "EILSEQ", "ERANGE", "errno",
    -- float.h
    "DBL_DIG", "DBL_EPSILON", "DBL_MANT_DIG", "DBL_MAX", "DBL_MAX_10_EXP",
    "DBL_MAX_EXP", "DBL_MIN", "DBL_MIN_10_EXP", "DBL_MIN_EXP", "FLT_DIG",
    "FLT_EPSILON", "FLT_MANT_DIG", "FLT_MAX", "FLT_MAX_10_EXP", "FLT_MAX_EXP",
    "FLT_MIN", "FLT_MIN_10_EXP", "FLT_MIN_EXP", "FLT_RADIX", "FLT_ROUNDS",
    "LDBL_DIG", "LDBL_EPSILON", "LDBL_MANT_DIG", "LDBL_MAX", "LDBL_MAX_10_EXP",
    "LDBL_MAX_EXP", "LDBL_MIN", "LDBL_MIN_10_EXP", "LDBL_MIN_EXP",
    -- so646.h [Amendment 1] c++
    "and", "and_eq", "bitand", "bitor", "compl", "not", "not_eq", "or",
    "or_eq", "xor", "xor_eq",
    -- limits.h
    "CHAR_BIT", "CHAR_MAX", "CHAR_MIN", "INT_MAX", "INT_MIN", "LONG_MAX",
    "LONG_MIN", "MB_LEN_MAX", "SCHAR_MAX", "SCHAR_MIN", "SHRT_MAX",
    "SHRT_MIN", "UCHAR_MAX", "UINT_MAX", "ULONG_MAX", "USHRT_MAX",
    -- locale.h
    "LC_ALL", "LC_COLLATE", "LC_CTYPE", "LC_MONETARY", "LC_NUMERIC", "LC_TIME",
    "lconv", "localeconv", "setlocale",
    -- math.h
    "HUGE_VAL", "abs", "acos", "acosf", "acosl", "asin", "asinf", "asinl",
    "atan", "atanf", "atanl", "atan2", "atan2f", "atan2l", "ceil", "ceilf",
    "ceill", "cos", "cosf", "cosl", "cosh", "coshf", "coshl", "exp", "expf",
    "expl", "fabs", "fabsf", "fabsl", "floor", "floorf", "floorl", "fmod",
    "fmodf", "fmodl", "frexp", "frexpf", "frexpl", "ldexp", "ldexpf", "ldexpl",
    "log", "logf", "logl", "log10", "log10f", "log10l", "modf", "modff",
    "modfl", "pow", "powf", "powl", "sin", "sinf", "sinl", "sinh", "sinhf",
    "sinhl", "sqrt", "sqrtf", "sqrtl", "tan", "tanf", "tanl", "tanh", "tanhf",
    "tanhl",
    -- setjmp.h
    "jmp_buf", "longjmp", "setjmp",
    -- signal.h
    "SIGABRT", "SIGFPE", "SIGILL", "SIGINT", "SIGSEGV", "SIGTERM", "SIG_DFL",
    "SIG_ERR", "SIG_IGN", "raise", "sig_atomic_t", "signal",
    -- stdarg.h
    "va_arg", "va_end", "va_list", "va_start",
    -- stddef.h
    "NULL", "offsetof", "ptrdiff_t", "size_t", "wchar_t",
    -- stdio.h
    "_IOFBF", "_IOLBF", "_IONBF", "BUFSIZ", "EOF", "FILE", "FILENAME_MAX",
    "FOPEN_MAX", "L_tmpnam", "SEEK_CUR", "SEEK_END", "SEEK_SET", "TMP_MAX",
    "clearerr", "fclose", "feof", "ferror", "fflush", "fgetc", "fgetpos",
    "fgets", "fopen", "fpos_t", "fprintf", "fputc", "fputs", "fread", "freopen",
    "fscanf", "fseek", "fsetpos", "ftell", "fwrite", "getc", "getchar", "gets",
    "perror", "printf", "putc", "putchar", "puts", "remove", "rename", "rewind",
    "scanf", "setbuf", "setvbuf", "sprintf", "sscanf", "stderr", "stdin",
    "stdout", "tmpfile", "tmpnam", "ungetc", "vfprintf", "vprintf", "vsprintf",
    -- stdlib.h
    "EXIT_FAILURE", "EXIT_SUCCESS", "MB_CUR_MAX", "RAND_MAX", "abort", "abs",
    "atexit", "atof", "atoi", "atol", "bsearch", "calloc", "div", "div_t",
    "exit", "free", "getenv", "labs", "ldiv", "ldiv_t", "malloc", "mblen",
    "mbstowcs", "mbtowc", "qsort", "rand", "realloc", "srand", "strtod",
    "strtol", "strtoul", "system", "wcstombs", "wctomb",
    -- string.h
    "memchr", "memcmp", "memcpy", "memmove", "memset", "strcat", "strchr",
    "strcmp", "strcoll", "strcpy", "strcspn", "strerror", "strlen", "strncat",
    "strncmp", "strncpy", "strpbrk", "strrchr", "strspn", "strstr", "strtok",
    "strxfrm",
    -- time.h
    "CLOCKS_PER_SEC", "asctime", "clock", "clock_t", "ctime", "difftime",
    "gmtime", "localtime", "mktime", "strftime", "time", "time_t", "tm",
    -- wchar.h [Amendment 1]
    "btowc", "fgetwc", "fgetws", "fputwc", "fputws", "fwide", "fwprintf",
    "fwscanf", "getwc", "getwchar", "mbrlen", "mbrtowc", "mbsinit",
    "mbsrtowcs", "mbstate_t", "putwc", "putwchar", "swprintf", "swscanf", "tm",
    "ungetwc", "vfwprintf", "vswprintf", "vwprintf", "WCHAR_MAX", "WCHAR_MIN",
    "wcrtomb", "wcscat", "wcschr", "wcscmp", "wcscoll", "wcscpy", "wcscspn",
    "wcsftime", "wcslen", "wcsncat", "wcsncmp", "wcsncpy", "wcspbrk", "wcsrchr",
    "wcsrtombs", "wcsspn", "wcsstr", "wcstod", "wcstok", "wcstol", "wcstoul",
    "wcsxfrm", "wctob", "WEOF", "wint_t", "wmemchr", "wmemcmp", "wmemcpy",
    "wmemmove", "wmemset", "wprintf", "wscanf",
    -- wctype.h [Amendment 1]
    "iswalnum", "iswalpha", "iswcntrl", "iswctype", "iswdigit", "iswgraph",
    "iswlower", "iswprint", "iswpunct", "iswspace", "iswupper", "iswxdigit",
    "towctrans", "towlower", "towupper", "wctrans", "wctrans_t", "wctype",
    "wctype_t", "wint_t",
    -- preprocessor predefined macros
    "__DATE__", "__FILE__", "__LINE__", "__STDC__", "__STDC_VERSION__",
    "__TIME__"}

global constant c_user_words = { -- used for C preproccesor
    "#define", "#include", "#undef", "#ifndef", "#endif", "#elif", "#else",
    "#if", "#ifdef", "#warning", "#error", "#pragma"}



--bookmark: MS-DOS Qbasic 1.1 (UPPER case); ---------------------------------

global constant qbasic_keywords = {
    "ABSOLUTE", "ACCESS", "AND", "APPEND", "AS", "BINARY", "CASE", "DO",
    "DYNAMIC","ELSE", "ELSEIF", "END", "EXIT", "EQV", "FOR", "FUNCTION", "IF",
    "IMP", "IS", "LIST", "LOOP", "MOD", "NEXT", "NOT", "OR", "OUTPUT", "RANDOM",
    "SELECT", "STATIC", "STEP", "SUB", "THEN", "TO", "TYPE", "UNTIL", "USING",
    "WEND", "WHILE", "XOR"}

global constant qbasic_builtins = {
    "ABS", "ANY", "ASC", "ATN", "BASE", "BEEP", "BLOAD", "BSAVE", "CALL",
    "CDBL", "CHAIN", "CHDIR", "CHR", "CINT", "CIRCLE", "CLEAR", "CLNG", "CLOSE",
    "CLS", "COLOR", "COM", "COMMON", "CONST", "COS", "DOUBLE", "CSNG", "CSRLIN",
    "CVD", "CVDMBF", "CVI", "CVL", "CVS", "CVSMBF", "DATA", "DATE", "DECLARE",
    "DEF", "FN", "SEG", "DEFDBL", "DEFINT", "DEFLNG", "DEFSNG", "DEFSTR", "DIM",
    "DRAW", "ENVIRON", "EOF", "ERASE", "ERDEV", "ERL", "ERR", "ERROR", "EXP",
    "FIELD", "FILEATTR", "FILES", "FIX", "FRE", "FREEFILE", "GET", "GOSUB",
    "GOTO", "HEX", "INKEY", "INP", "INPUT", "INSTR", "INT", "INTEGER", "IOCTL",
    "KEY", "KILL", "LBOUND", "LCASE", "LEFT", "LEN", "LET", "LINE", "LOC",
    "LOCATE", "LOCK", "LOF", "LOG", "LONG", "LPOS", "LPRINT", "LSET", "LTRIM",
    "MID", "MKD", "MKDIR", "MKDMBF", "MKI", "MKL", "MKS", "MKSMBF", "NAME",
    "OCT", "OFF", "ON", "OPEN", "OPTION", "OUT", "PAINT", "PALETTE", "PCOPY",
    "PEEK", "PEN", "PLAY", "PMAP", "POINT", "POKE", "POS", "PRESET", "PRINT",
    "PSET", "PUT", "RANDOMIZE", "READ", "REDIM", "REM", "RESET", "RESTORE",
    "RESUME", "RETURN", "RIGHT", "RMDIR", "RND", "RSET", "RTRIM", "RUN",
    "SCREEN", "SEEK", "SGN", "SHARED", "SHELL", "SIN", "SINGLE", "SLEEP",
    "SOUND", "SPACE", "SPC", "STRING", "SQR", "STICK", "STOP", "STR", "STRIG",
    "SWAP", "SYSTEM", "TAB", "TAN", "TIME", "TIMER", "TROFF", "TRON", "UBOUND",
    "UCASE", "UNLOCK", "VAL", "VARPTR", "VARSEG", "VIEW", "WAIT", "WIDTH",
    "WINDOW", "WRITE"}

global constant qbasic_libraries = {}

-- few keywords from MS-QuickBASIC 4.5/VBDOS 1.0:
global constant qbasic_user_words = {
    "BYVAL", "ALIAS", "PRESERVE", "EXPLICIT"}

