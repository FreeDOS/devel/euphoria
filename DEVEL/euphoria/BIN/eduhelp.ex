-- FILE:     EDUHELP.EX
-- PURPOSE:  Index Euphoria & Lib2 Keywords for faster edu-help (control-K)
-- AUTHOR:   Shian Lee
-- VERSION:  1.00  February/21/2022
-- PLATFORM: Any
-- NOTE:    * *You should run this file after updating any '.doc' file.*
--          * See also in edu.ex: get_help_index() & context_sensitive_help().
-- HISTORY:  v1.00 - initial version (February/21/2022).
-----------------------------------------------------------------------------

include get.e
include file2.e
include machine2.e


--bookmark: *.doc files to index in euphoria\doc;

-- Place doc files with most commonly used words first.
-- New *.doc files in euphoria\include\ with "---<word>---" add to this list.
constant DOC_FILES = {
    "library.doc", "machine2.doc", "database.doc",
    "string.doc", "math.doc", "datetime.doc",
    "file2.doc", "random.doc", "utf8.doc", "japi.doc"
}


--bookmark: Start;

procedure fatal(sequence msg)
    puts(1, "\n\n\t*** " & msg & " ***\n")
    puts(1, "\tCannot continue.\n")
    pause()
    abort(1)
end procedure


clear_screen()


-- edu.hlp pathname
constant HLP_FILE = full_pathname("edu.hlp")

if atom(HLP_FILE) then
    fatal("Bad file pathname '" & HLP_FILE & "'!")
end if


if not floor(prompt_number("\n Re-index '" & HLP_FILE & "' [0,1]? ", {0, 1}))
then
    abort(0) -- user entered 0, do nothing
end if


constant EUDIR = getenv("EUDIR")

if atom(EUDIR) then
    fatal("Please Set 'EUDIR' and try again!")
end if


constant PATH = slash_path(EUDIR, TRUE) & "doc" & OS_SLASH

-- make sure all doc files exist and init out sequence
sequence file, out
out = EMPTY
for i = 1 to length(DOC_FILES) do
    file = DOC_FILES[i]
    if atom(dir(PATH & file)) then
	fatal("'" & PATH & file & "' not found!")
    end if
    -- out = {doc file name, {"word","word",...}, {line,line,...}}
    out = append(out, {file, EMPTY, EMPTY}) 
end for


-- create/update the index file in current directory
integer fn, p, p2, line_num
object line
sequence word, total

total = repeat(0, length(DOC_FILES) + 1) -- init statistics to 0

clear_screen()
tick_rate(100) -- for delay()
for i = 1 to length(DOC_FILES) do
    file = DOC_FILES[i]
    fn = open(PATH & file, "r")
    if fn = -1 then
	fatal("Can't open '" & PATH & file & "' for reading!")
    end if
    
    -- scan entire doc file for keywords ("---<keyword>---")
    line_num = 0
    while TRUE do
	line = gets(fn)
	if atom(line) then
	    close(fn)
	    exit
	end if
	
	line_num += 1
	p = match("---<", line)
	if p then
	    p2 = match_from(">---", line, p + 4)
	    if p2 then
		word = line[p + 4..p2 - 1]
		if length(word) then
		    -- {doc file name, {"word","word",...}, {line,line,...}}
		    out[i][2] &= {word}
		    out[i][3] &= line_num
		    total[i] += 1
		    puts(1, word & ' ') -- show word on screen
		end if
	    end if
	end if
    end while
    
    total[$] += total[i] -- save sum of totals
    delay(30)
end for

-- save index in file
fn = open(HLP_FILE, "wb")
if fn = -1 then
    fatal("Can't open '" & HLP_FILE & "' for writing!")
end if

constant KEY = {1,2,3} -- same key must be used in edu.ex

-- using sencode() because:
--  less human mistakes can happen to binary file then to text file.
puts(fn, sencode(sprint(out), KEY)) 
close(fn)


-- print total on the screen
puts(1, "\n\nDone. '" & HLP_FILE & "' re-indexed successfully.\n\n")
for i = 1 to length(DOC_FILES) do
    printf(1, " %2d. %-15s %8d keywords found.\n", {i, DOC_FILES[i], total[i]}) 
end for
printf(1, "\n %d Keywords found in %d Files.\n", {total[$], length(total) - 1})


-- End of file.
