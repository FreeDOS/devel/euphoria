@echo off
@rem  run edu.ex for Windows-10
@rem  Windows-10 don't support the 16-bit ex.exe interpreter.

@rem  you could also try:  exw.exe edu.ex %1 %2 %3

@rem run edu for Windows-10:
exwc edu.ex %1 %2 %3

