@ECHO OFF
@REM compile and run c file from edu.ex, for DOS/Windows
@REM tested on FreeDOS 1.2 with DJGPP 2.05

gcc -Wall %1 -o a.exe

@REM exit on gcc error
IF ERRORLEVEL 1 GOTO error

.\a.exe
GOTO end

:error
@rem PAUSE

:end
